Marceline
======

A happy theme for [Ghost](https://github.com/tryghost/ghost/) for the La Factoria's blog

## Features

* Responsive design
* Icons provided by [Font-Awesome](https://github.com/FortAwesome/Font-Awesome).
* Syntax highlighting using [Highlight.js](https://github.com/isagalaev/highlight.js)
* Infinite Scroll using [ghost-blog-infinite-scroll](https://github.com/bateuanave/ghost-blog-infinite-scroll)

## License

Based on Marceline Theme View Marceline in action [here](http://germanencinas.com/marceline) by [Germán Encinas](http://germanencinas.com/).
Copyright (c) 2015 Germán Encinas - Released under The MIT License.
